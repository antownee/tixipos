﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TixiPOS.core.Models
{
    public class CheckoutItem
    {
        private Item item;
        private int count;
        private int total;

        public Item Item
        {
            get
            {
                return item;
            }

            set
            {
                item = value;
            }
        }

        public int Count
        {
            get
            {
                return count;
            }

            set
            {
                count = value;
            }
        }

        public int Total
        {
            get
            {
                total = Count * Item.Price;
                return total;
            }
        }
    }
}
