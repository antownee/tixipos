﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TixiPOS.core.Models
{
    public class Purchase
    {
        private string _userID;
        private int amount;
        private string readerID;
        private int prebalance;
        private int postbalance;
        private string merchantID;
        private List<Item> itemsBought;

        public string UserID
        {
            get
            {
                return _userID;
            }

            set
            {
                _userID = value;
            }
        }

        public int Amount
        {
            get
            {
                return amount;
            }

            set
            {
                amount = value;
            }
        }

        public string ReaderID
        {
            get
            {
                return readerID;
            }

            set
            {
                readerID = value;
            }
        }

        public string MerchantID
        {
            get
            {
                return merchantID;
            }

            set
            {
                merchantID = value;
            }
        }

        public List<Item> ItemsBought
        {
            get
            {
                return itemsBought;
            }

            set
            {
                itemsBought = value;
            }
        }

        public int Prebalance
        {
            get
            {
                return prebalance;
            }

            set
            {
                prebalance = value;
            }
        }

        public int Postbalance
        {
            get
            {
                return postbalance;
            }

            set
            {
                postbalance = value;
            }
        }
    }
}
