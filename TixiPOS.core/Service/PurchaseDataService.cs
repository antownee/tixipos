﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TixiPOS.core.Models;

namespace TixiPOS.core.Service
{
    public class PurchaseDataService
    {
        public async Task<string> PostTransaction(Purchase purchase)
        {
            var client = new RestClient("http://192.168.1.200:3000"); //BaseURL
            var request = new RestRequest("api/purchases/create", Method.POST);


            var purchaseJson = request.JsonSerializer.Serialize(purchase);
            request.RequestFormat = DataFormat.Json;

            request.AddParameter("application/json", purchaseJson, ParameterType.RequestBody);

            var restResponse = await client.ExecuteTaskAsync(request);

            return restResponse.Content;
        }

        //public async Task PostToServer(List<CheckoutItem> lstItems)
        //{
        //    //url
        //    //instantiate and use url
        //    var url = "http://192.168.1.200:3000";
        //    var d = new Dictionary<string, object>();
        //    d.Add("uID", 6768586586);
        //    d.Add("rID", "RDR-0017");
        //    d.Add("mID", "MRCH-0017");
        //    var total = lstItems.Sum(a => a.Total);
        //    d.Add("amnt", total);
        //    d.Add("bal", 200);
        //    var itms = from i in lstItems
        //               select i.Item;
        //    var d2 = new Dictionary<string, object>();
        //    foreach (var k in itms)
        //    {
        //        d2.Add(k.ItemName, k.Price);
        //    }
        //    d.Add("itms", d2);

        //    var jsonReq = JsonConvert.SerializeObject(d);
        //    var content = new StringContent(jsonReq, UTF8Encoding.UTF8, "application/json");

        //    using (var hc = new HttpClient())
        //    {
        //        try
        //        {
        //            var res = await hc.PostAsync(url, content);

        //            res.EnsureSuccessStatusCode();
        //        }
        //        catch (Exception)
        //        {
        //            throw;
        //        }
        //    }
        //}


    }
}
