﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TixiPOS.core.Models;
using TixiPOS.core.Repository;

namespace TixiPOS.core.Service
{
    public class ItemDataService
    {
        private static ItemsRepository itemRepo = new ItemsRepository();

        public List<ItemGroup> GetItemByGroup()
        {
            return itemRepo.GetItemsByGroup();
        }

        public List<Item> GetAllItems()
        {
            return itemRepo.GetAllItems();
        }
    }
}
