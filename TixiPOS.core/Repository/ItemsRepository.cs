﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TixiPOS.core.Models;

namespace TixiPOS.core.Repository
{
    public class ItemsRepository
    {
        private static List<ItemGroup> ItemGroupsList = new List<ItemGroup>()
        {
            new ItemGroup()
            {
                Title = "Drinks",
                GroupID = 1,
                Items = new List<Item>()
                {
                    new Item
                    {
                        ItemID = 101,
                        ItemName = "Jameson 1L",
                        Price = 1000
                    },

                    new Item
                    {
                        ItemID = 102,
                        ItemName = "Tusker",
                        Price = 200
                    },
                    new Item
                    {
                        ItemID = 103,
                        ItemName = "Jack Daniel",
                        Price = 1500
                    },
                    new Item
                    {
                        ItemID = 104,
                        ItemName = "Fresh Juice",
                        Price = 150
                    }
                }
            },
            new ItemGroup()
            {
                GroupID = 2,
                Title = "Snacks",
                Items = new List<Item>()
                {
                    new Item
                    {
                        ItemID = 105,
                        ItemName = "Burger",
                        Price = 750
                    },
                    new Item
                    {
                        ItemID = 106,
                        ItemName = "Fries",
                        Price = 300
                    },
                    new Item
                    {
                        ItemID = 107,
                        ItemName = "Hotdog",
                        Price = 400
                    }
                }
            }

        };

        public List<ItemGroup> GetItemsByGroup()
        {
            return ItemGroupsList;
        }

        public List<Item> GetAllItems()
        {
            IEnumerable<Item> items =
                from itmGroup in ItemGroupsList
                from itm in itmGroup.Items
                select itm;

            return items.ToList<Item>();
        }
    }
}
