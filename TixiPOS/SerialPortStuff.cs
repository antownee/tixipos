using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace TixiPOS
{
    class SerialPortStuff
    {
        //public void SerialPortInit()
        //{
        //    mUsbSerialDeviceManager = new UsbSerialDeviceManager(this, "TixiPOS.TixiPOS.TixiPOS.TixiPOS.USB_PERMISSION", true);
        //    mUsbSerialPortList = new List<UsbSerialPort>();

        //    mUsbSerialDeviceManager.DeviceAttached += (o, e) => { DeviceAttached(); };
        //    mUsbSerialDeviceManager.DeviceDetached += (o, e) => { DeviceDetached(); };
        //    mUsbSerialDeviceManager.Start();
        //}

        //public void DeviceDetached()
        //{
        //    //On device detach, tell me. Toast
        //    mUsbSerialPort.DataReceived -= MUsbSerialPort_DataReceived;
        //    mUsbSerialPort.Close();
        //    Toast.MakeText(this, "Device detached", ToastLength.Short).Show();
        //}

        //public void DeviceAttached()
        //{
        //    //On device attach, tell me. Toast? 
        //    var devices = mUsbSerialDeviceManager.AttachedDevices;

        //    //Set the port
        //    mUsbSerialPort = devices.FirstOrDefault().Ports.FirstOrDefault();
        //    mUsbSerialPort.Baudrate = 57600;
        //    mUsbSerialPort.Parity = Parity.None;
        //    mUsbSerialPort.StopBits = StopBits.One;
        //    mUsbSerialPort.DataBits = 8;
        //    mUsbSerialPort.Rts = true;
        //    mUsbSerialPort.DataReceived += MUsbSerialPort_DataReceived;

        //    mUsbSerialPort.Open();
        //    //Toast.MakeText(this, string.Format("{0};{1};{2}", usbDevice.VendorId, usbDevice.ProductId, usbDevice.DeviceId), ToastLength.Long).Show();
        //    Toast.MakeText(this, "Device connected", ToastLength.Long).Show();
        //}


        //private void BtnPay_Click(object sender, EventArgs e)
        //{
        //    //First display popup 
        //    ShowAlertDialog("Present tag for scanning");

        //    //Set checkout total
        //    checkoutTotal = checkoutItmList.Sum(a => a.Total);

        //    //0x0A - Line Feed or \n    0x0D - Carriage return or \r
        //    //Send read command Full command - 32,4 \r\n
        //    //0x33 0x32 0x2c 0x34 is "32,4" convert it into hex
        //    // 0x33, 0x32, 0x2c, 0x34, 0x0D, 0x0A 

        //    //Update balance balance commmand
        //    //"52,<Amount>\r\n"
        //    if (mUsbSerialPort != null)
        //    {
        //        byte[] cmd = Encoding.Default.GetBytes(string.Format("52,{0}\r\n", checkoutTotal));
        //        mUsbSerialPort.Write(cmd, 1000);
        //        Toast.MakeText(this, Encoding.Default.GetString(cmd), ToastLength.Long).Show();
        //    }
        //}

        //private void MUsbSerialPort_DataReceived(object sender, DataReceivedEventArgs e)
        //{
        //    byte[] data = new byte[1024 * 16];
        //    //byte[] data = new byte[1024 * 16];
        //    int length = e.Port.Read(data, 0);
        //    RunOnUiThread(() => CleanUpReceivedData(data, length));
        //}

        //public void CleanUpReceivedData(byte[] data, int length)
        //{
        //    sb.Append(Encoding.Default.GetString(data, 0, length));

        //    //If string contains at least 2 instances of "\r\n", then we're good
        //    //Count occurences            
        //    int occ = (sb.ToString().Length - sb.ToString().Replace("\r\n", "").Length) / "\r\n".Length;

        //    if (occ > 1)
        //    {
        //        //Parse data
        //        string[] splits = sb.ToString().Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

        //        if (splits[splits.Count() - 1] != "END")
        //        {
        //            lstFrame.Clear();
        //            //Add to waiting array
        //            foreach (var s in splits)
        //            {
        //                lstFrame.Add(s);
        //            }
        //        }
        //        else if (splits.Count() == 3 && splits[splits.Count() - 1] == "END" && splits[0].Contains("UID"))
        //        {
        //            //Normal frame. Either 401 or 404
        //            lstFrame.Clear();
        //            foreach (var k in splits)
        //            {
        //                lstFrame.Add(k);
        //            }
        //        }
        //        else if (splits.Count() == 3 && splits[splits.Count() - 1] == "END")
        //        {
        //            //This is the second half of frame.
        //            //Add it to the first half
        //            //Add END to splits
        //            foreach (var k in splits)
        //            {
        //                lstFrame.Add(k);
        //            }
        //        }
        //        else
        //        {
        //            lstFrame.Clear();

        //            //We have a complete frame
        //            foreach (var f in splits)
        //            {
        //                lstFrame.Add(f);
        //            }
        //        }


        //        //Do stuff with the complete lstFrame
        //        switch (lstFrame.Count)
        //        {
        //            case 3:
        //                //401 and 404
        //                if (lstFrame[1].Contains("401"))
        //                {
        //                    //Ignore
        //                }
        //                else if (lstFrame[1].Contains("404"))
        //                {
        //                    Toast.MakeText(this, "Please tap card again.", ToastLength.Long).Show();
        //                }
        //                break;
        //            case 4:
        //                //Error frame from reader
        //                if (lstFrame[3].Contains("CAN'T BE"))
        //                {
        //                    Toast.MakeText(this, "Can't bill items worth KES 0", ToastLength.Long).Show();
        //                }
        //                break;
        //            case 5:
        //                if (lstFrame[3] == "INSUFFICIENT BALANCE")
        //                {
        //                    Toast.MakeText(this, string.Format("INSUFFICIENT {0}", lstFrame[2]), ToastLength.Long).Show();
        //                }
        //                else if (lstFrame[3].Contains("NEW BALANCE"))
        //                {
        //                    //We are in!!
        //                    //Get UID
        //                    var uid = lstFrame[0];
        //                    //Get new balance and what we've charged
        //                    var oldBalance = lstFrame[2].Split(':')[1].Split(' ')[1];
        //                    var newBalance = lstFrame[3].Split(':')[1].Split(' ')[1];
        //                    var cost = Convert.ToInt32(oldBalance) - Convert.ToInt32(newBalance);

        //                    ShowAlertDialog(string.Format("KES {0} charged. \nNew balance: {1}", cost, newBalance));

        //                    //SAVE TO FILE
        //                    //SEND REQUEST TO SERVER
        //                    //SaveTransacion(uid, cost, Convert.ToInt32(newBalance)); 
        //                }
        //                else if (lstFrame[3].Contains("CAN'T BE"))
        //                {
        //                    Toast.MakeText(this, "Can't bill items worth KES 0", ToastLength.Long).Show();
        //                }
        //                break;
        //            default:
        //                break;
        //        }

        //        //Clear string builder if frame is complete
        //        sb.Clear();
        //    }
        //}

        //public void ShowAlertDialog(string message)
        //{
        //    AlertDialog.Builder alert = new AlertDialog.Builder(this);
        //    if (d != null)
        //    {
        //        d.Cancel();
        //    }
        //    alert.SetMessage(message);
        //    alert.SetPositiveButton("Close", (senderArg, args) =>
        //    {
        //        //
        //    });
        //    d = alert.Show();
        //    d.Show();
        //}

    }
}