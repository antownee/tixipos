using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Lang;
using TixiPOS.core.Models;

namespace TixiPOS.Adapter
{
    public class ItemGridAdapter: BaseAdapter
    {
        List<Item> itms;
        Activity context;

        public ItemGridAdapter(Activity context, List<Item> itms)
        {
            this.context = context;
            this.itms = itms;
        }


        public override Java.Lang.Object GetItem(int position)
        {
            return null;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var itm = itms[position];

            //Times the item has been clicked
            int count = 1;

            if (convertView == null)
            {
                convertView = context.LayoutInflater.Inflate(Resource.Layout.GridItem, null);
            }

            Button btnItemName = convertView.FindViewById<Button>(Resource.Id.gridItemName);

            btnItemName.Text = itm.ItemName;

            if (!btnItemName.HasOnClickListeners)
            {
                btnItemName.Click += (o, e) =>
                {
                    btnItemName.Text = string.Format("{0} - {1}", itm.ItemName, count++);

                    var k = Menu.checkoutItmList.Where(a => a.Item.ItemID == itm.ItemID).FirstOrDefault();
                    if (k != null)
                    {
                        k.Count++;
                    }
                    else
                    {
                        Menu.checkoutItmList.Add(new CheckoutItem
                        {
                            Item = itm,
                            Count = 1
                        });
                    }
                    Menu.checkoutListAdapter.NotifyDataSetChanged();
                    //Toast.MakeText(context, itm.ItemName, ToastLength.Short);
                };
            }
            return convertView;
        }


        public override int Count
        {
            get
            {
                return itms.Count;
            }
        }
    }
}