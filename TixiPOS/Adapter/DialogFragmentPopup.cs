using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace TixiPOS.Adapter
{
    public class DialogFragmentPopup : DialogFragment
    {
        public static string Message { get; set; }
        public static DialogFragmentPopup NewInstance(Bundle bundle, string message)
        {
            DialogFragmentPopup fragment = new DialogFragmentPopup();
            Message = message;
            fragment.Arguments = bundle;
            return fragment;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            View view = inflater.Inflate(Resource.Layout.PayPopup, container, false);
            TextView txtContent = view.FindViewById<TextView>(Resource.Id.txtPopupContent);
            Button btnClose = view.FindViewById<Button>(Resource.Id.btnPopupClose);
            Menu.IsDialogClosed = false;

            txtContent.Text = Message;
            btnClose.Click += (o,e) => 
            {
                Menu.IsDialogClosed = true;
                Dismiss();
            };

            return view;
        }
    }
}