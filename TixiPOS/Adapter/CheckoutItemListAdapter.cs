using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Lang;
using TixiPOS.core.Models;

namespace TixiPOS.Adapter
{
    public class CheckoutItemListAdapter : BaseAdapter
    {
        List<CheckoutItem> itms;
        Activity context;
        
        public CheckoutItemListAdapter(Activity context, List<CheckoutItem> itms)
        {
            this.context = context;
            this.itms = itms;
        }
        public override int Count
        {
            get
            {
                return itms.Count;
            }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return null;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var itm = itms[position];
     
            if (convertView == null)
            {
                convertView = context.LayoutInflater.Inflate(Resource.Layout.CheckoutItemRow, null);
            }

            TextView txtItemPosition = convertView.FindViewById<TextView>(Resource.Id.txtCheckoutItemPosition);
            TextView txtItemName = convertView.FindViewById<TextView>(Resource.Id.txtCheckoutItemName);
            TextView txtItemPrice = convertView.FindViewById<TextView>(Resource.Id.txtCheckoutItemPrice);
            Button btnDeleteItem = convertView.FindViewById<Button>(Resource.Id.btnCheckoutItemDelete);
            btnDeleteItem.SetTag(Resource.Id.btnCheckoutItemDelete, position);

            if (!btnDeleteItem.HasOnClickListeners) //Avoid multiclicking
            {
                btnDeleteItem.Click += (o, e) =>
                {
                    var pos = (int)((Button)o).GetTag(Resource.Id.btnCheckoutItemDelete);
                    var i = Menu.checkoutItmList[pos];
                    //Delete item/ Lessen the count
                    i.Count--;
                    if (i.Count < 1)
                    {
                        //Remove from list
                        Menu.checkoutItmList.Remove(i);
                    }
                    else
                    {
                        //Edit field to show lessened 
                        Menu.checkoutItmList.Where(a => a.Item.ItemID == i.Item.ItemID).FirstOrDefault().Count = i.Count;
                    }
                    Menu.checkoutListAdapter.NotifyDataSetChanged();
                    UpdatePayButton();
                };
            }


            //UPDATE PAY BUTTON
            UpdatePayButton();
            txtItemPosition.Text = (position + 1).ToString();
            txtItemName.Text = string.Format("{0} * {1}", itm.Item.ItemName,itm.Count);
            txtItemPrice.Text = itm.Total.ToString();

            return convertView;
        }

        public void UpdatePayButton()
        {
            Button btnPay = ((Menu)context).FindViewById<Button>(Resource.Id.btnCheckoutAmount);
            var total = Menu.checkoutItmList.Sum(a => a.Total);
            btnPay.Text = string.Format("PAY: {0}", total);
        }
    }
}