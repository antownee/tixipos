using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using TixiPOS.core.Service;
using TixiPOS.core.Models;
using TixiPOS.Adapter;
using Newtonsoft.Json;
using System.IO;
using Android.Hardware.Usb;
using System.Threading.Tasks;
using Aid.UsbSerial;
using Com.Acs.Smartcard;
using System.Security.Cryptography;
using uPLibrary.Networking.M2Mqtt;
using System.Net;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace TixiPOS
{
    [Activity(Label = "TixiPOS", MainLauncher = true)]
    [IntentFilter(new[] { UsbManager.ActionUsbDeviceAttached })]
    [MetaData(UsbManager.ActionUsbDeviceAttached, Resource = "@xml/device_filter")]
    public class Menu : Activity
    {
        private ItemDataService itmdataService;
        private PurchaseDataService prchdataService;
        private List<Item> itmList;
        public static List<CheckoutItem> checkoutItmList;
        private GridView itemGridView;
        private ListView itemCheckoutListView;
        public static CheckoutItemListAdapter checkoutListAdapter;
        public static bool IsDialogClosed = true;
        public DialogFragmentPopup newFragment;
        private int checkoutTotal = 0;
        private Dialog d;
        private const string AUTH_FAIL_MSG = "Invalid card. Authentication failed.";
        private const string MQTT_BROKER_IP = "192.168.137.1";
        private MqttClient client;

        //Serial port stuff
        private List<UsbSerialPort> mUsbSerialPortList;
        private UsbSerialDeviceManager mUsbSerialDeviceManager;
        private UsbSerialPort mUsbSerialPort;
        private StringBuilder sb;
        private List<string> lstFrame;


        //Card reader setup
        private static Reader reader;
        private static UsbDevice acsReader;
        public static bool cardPlaced = false;
        public static bool cardRemoved = false;
        private static string ACTION_USB_PERMISSION = "TixiPOS.TixiPOS.TixiPOS.TixiPOS.USB_PERMISSION";
        private int readerDuty = 0;

        public Menu()
        {
            lstFrame = new List<string>();
            sb = new StringBuilder();
            checkoutItmList = new List<CheckoutItem>();
            prchdataService = new PurchaseDataService();
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.Menu);

            //Get data from our repo
            itmdataService = new ItemDataService();
            itmList = itmdataService.GetAllItems();

            //GridView
            itemGridView = FindViewById<GridView>(Resource.Id.itemGridView);
            itemGridView.Adapter = new ItemGridAdapter(this, itmList);

            //Checkout List View
            itemCheckoutListView = FindViewById<ListView>(Resource.Id.itemCheckoutListView);

            //List of added items
            checkoutListAdapter = new CheckoutItemListAdapter(this, checkoutItmList);
            itemCheckoutListView.Adapter = checkoutListAdapter;

            //Pay button and it's event handler
            Button btnPay = FindViewById<Button>(Resource.Id.btnCheckoutAmount);
            btnPay.Click += BtnPay_Click;

            //Configuring MQTT
            configureMQTT();

            //Card reader initialisation
            initCardReader();
        }

        private void BtnPay_Click(object sender, EventArgs e)
        {
            //First display popup 
            ShowAlertDialog("Present tag for scanning");
            
            //Set checkout total
            checkoutTotal = checkoutItmList.Sum(a => a.Total);

            //Open reader
            try
            {
                if(reader == null)
                {
                    Toast.MakeText(this, "Device is not connected", ToastLength.Short).Show();
                    return;
                }

                reader.Open(acsReader);
                //Set flag 
                readerDuty = 1; //Get balance and card number
                //Get cardNumber and balance
                //Do the balance remaining logic here
                //Service
                //SaveTransacion(cardNumber,checkoutTotal, balance from card)
            }
            catch (ReaderException exc)
            {
                Toast.MakeText(this, "Try reconnecting device", ToastLength.Short).Show();
                return;
                //throw;
            }
        }


        public void ShowAlertDialog(string message)
        {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            if (d != null)
            {
                d.Cancel();
            }
            alert.SetMessage(message);
            alert.SetPositiveButton("Close", (senderArg, args) =>
            {
                reader.Close();
                //Toast.MakeText(this, "Closing device", ToastLength.Short).Show();
                readerDuty = 1;
            });
            d = alert.Show();
            d.Show();
        }

        public void SaveTransacion(string UID, int checkoutTotal, int prebalance, int postbalance)
        {
            //Save to file.

            //Then open thread to send transaction to server
            //if okay, Toast make text
            var prchase = new Purchase()
            {
                UserID = UID,
                MerchantID = "MRCH-001", 
                ReaderID = "RDR-003",
                Prebalance = prebalance,
                Postbalance = postbalance,
                Amount = checkoutTotal,
                ItemsBought = CheckoutlistConversion(checkoutItmList)
            };

            string purchaseItems = JsonConvert.SerializeObject(prchase);
            var byt = Encoding.UTF8.GetBytes(purchaseItems);
            //Send using MQTT
            client.Publish("purchase", byt, MqttMsgBase.QOS_LEVEL_AT_LEAST_ONCE, true);
        }

        public void ShowDialog(string message)
        {
            FragmentTransaction ft = FragmentManager.BeginTransaction();
            ////Remove fragment else it will crash as it is already added to backstack
            Fragment prev = FragmentManager.FindFragmentByTag("dialog");
            if (prev != null)
            {
                ft.Remove(prev);
            }

            ft.AddToBackStack(null);

            // Create and show the dialog.
            newFragment = DialogFragmentPopup.NewInstance(null, message);
            newFragment.Show(ft, "dialog");
        }

        public List<Item> CheckoutlistConversion(List<CheckoutItem> lstChkout)
        {
            var a = new List<Item>();

            foreach (var item in lstChkout)
            {
                a.Add(new Item { ItemName = item.Item.ItemName, Price = item.Total });
            }

            return a;
        }

        public void initCardReader()
        {
            UsbManager manager = (UsbManager)GetSystemService(UsbService);
            reader = new Reader(manager);
            var keys = manager.DeviceList.Count;
            bool isDevice = manager.DeviceList.ContainsKey("/dev/bus/usb/001/003");
            IDictionary<String, UsbDevice> deviceList = manager.DeviceList;

            if (!isDevice)
            {
                Toast.MakeText(this, "No device connected", ToastLength.Short).Show();
                return;
            }
            Toast.MakeText(this, "Reader successfully connected", ToastLength.Short).Show();
            acsReader = manager.DeviceList["/dev/bus/usb/001/003"];

            PendingIntent mPermissionIntent = PendingIntent.GetBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
            IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
            UsbReciever usbReciever = new UsbReciever();
            RegisterReceiver(usbReciever, filter);

            manager.RequestPermission(acsReader, mPermissionIntent);
            bool isIn = manager.HasPermission(acsReader);

            reader.StateChange += Reader_StateChange;

        }

        private void Reader_StateChange(object sender, Reader.StateChangeEventArgs e)
        {
            cardPlaced = e.P1 == 1 ? true : false;
            cardRemoved = e.P1 > 1 ? true : false;

            if (cardRemoved)
            {
                return;
            }


            try
            {
                byte[] atr = reader.Power(0, Reader.CardWarmReset);

                //Authenticate. Quit if it fails
                var isAuth = authenticateCard();
                if(!isAuth)
                {
                    RunOnUiThread(() => Toast.MakeText(this, AUTH_FAIL_MSG, ToastLength.Short).Show());
                    return;
                }

                var j = getBalanceUid();
                if (j == null)
                {
                    RunOnUiThread(() => Toast.MakeText(this, "Unable to read balance.", ToastLength.Short).Show());
                    return;
                }
                //Is balance enough? Check
                if (checkoutTotal > j.balance)
                {
                    //var nb = 10000;
                    //byte[] newBalBytes = BitConverter.GetBytes(nb);
                    //writeMoneroToCard(newBalBytes);
                    RunOnUiThread(() => Toast.MakeText(this, "Insufficient funds. KES " + j.balance, ToastLength.Short).Show());
                    return;
                }
                else
                {
                    //Write to card with new balance
                    var newBl = j.balance - checkoutTotal;
                    //24 is the page number - 24 is hex
                    byte[] newBalBytes = BitConverter.GetBytes(newBl);
                    var isMonero = writeMoneroToCard(newBalBytes);
                    if (!isMonero)
                    {
                        RunOnUiThread(() => Toast.MakeText(this, "ERROR: Unable to update balance." + newBl, ToastLength.Long).Show());
                        return;
                    }
                    else
                    {
                        RunOnUiThread(() => Toast.MakeText(this, "Balance successfully updated. KES " + newBl, ToastLength.Short).Show());
                    }

                    //Send to server using MQTT!!
                    SaveTransacion(j.UID, checkoutTotal, j.balance, newBl);
                    return;
                }

                
                //Close reader
            }
            catch (RemovedCardException re)
            {
                RunOnUiThread(() => Toast.MakeText(this, "Scan again", ToastLength.Short).Show());
                return;
            }
            catch (UnpoweredCardException ue)
            {
                RunOnUiThread(() => Toast.MakeText(this, "Scan again", ToastLength.Short).Show());
                return;
            }
            catch (ProtocolMismatchException pe)
            {
                RunOnUiThread(() => Toast.MakeText(this, "Scan again", ToastLength.Short).Show());
                return;
            }
            catch (Exception ex)
            {
                RunOnUiThread(() => Toast.MakeText(this, ex.Message, ToastLength.Long).Show());
                throw;
            }
        }

        public bool authenticateCard()
        {
            byte[] Authcommand = { 0xFF, 0x00, 0x00, 0x00, 0x04, 0xD4, 0x42, 0x1A, 0x00 }; //Authenticate command that works. Read ACR122S pdf
            var res = sendReaderCommand(Authcommand);
            if (res.Length <= 10)
            {
                return false;
            }

            //Receive ekRndB
            var ekRndB = res.Substring(8, 16);
            var ekRndBbytes = ekRndB.ToHexBytes();
            byte[] FHDkey = { 0x48, 0x54, 0x53, 0x45, 0x52, 0x4f, 0x46, 0x45, 0x56, 0x49, 0x52, 0x44, 0x53, 0x4c, 0x4c, 0x49 };
            byte[] DEFkey = { 0x49, 0x45, 0x4D, 0x4B, 0x41, 0x45, 0x52, 0x42, 0x21, 0x4E, 0x41, 0x43, 0x55, 0x4F, 0x59, 0x46 };
            byte[] IV = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
            byte[] testekRndB = { 0x57, 0x72, 0x93, 0xFD, 0x2F, 0x34, 0xCA, 0x51 };
            //Obtain RndB
            var RndB = Decrypt3DES(FHDkey, ekRndBbytes, IV);
            //Generate RndA
            byte[] RndA = new byte[8];
            Random r = new Random();
            r.NextBytes(RndA);
            //Shift RndB to get RndB'
            byte[] RndB_ = new byte[8];
            for (int i = 0; i < RndB.Length - 1; i++)
            {
                RndB_[i] = RndB[i + 1];
            }

            RndB_[RndB.Length - 1] = RndB[0];
            //Concatenate RndA and RndB' to get RndA||RndB'
            byte[] RndARndB_ = new byte[RndA.Length + RndB_.Length];
            RndA.CopyTo(RndARndB_, 0);
            RndB_.CopyTo(RndARndB_, RndA.Length);
            //Encrypt to get ek(RndA||RndB') using ekRndBbytes as IV
            var ekRndaRndB = Encrypt3DES(FHDkey, RndARndB_, ekRndBbytes);
            //Send ekRndaRndB to reader and await ekRndA'
            byte[] seccommand = { 0xFF, 0x00, 0x00, 0x00, 0x13, 0xD4, 0x42, 0xAF }; //Respond to PICC. Read ACR122S pdf
            byte[] com = new byte[seccommand.Length + ekRndaRndB.Length];
            seccommand.CopyTo(com, 0);
            ekRndaRndB.CopyTo(com, seccommand.Length);
            byte[] resp = new byte[300];
            int respLength = reader.Transmit(0, com, com.Length, resp, resp.Length);
            var resps = resp.ToHexString().Substring(0, respLength * 2);
            //Decrypt ekRndA' to get RndA'
            if (resps.Length <= 10)
            {
                return false;
            }
            //Receive ekRndB
            var ekRndA_ = resps.Substring(8, 16).ToHexBytes();
            //Half of ekRndaRndB is the IV to use
            byte[] RndA_IV = new byte[8];
            RndA_IV = ekRndaRndB.ToHexString().Substring(16).ToHexBytes();
            var RndA_ = Decrypt3DES(FHDkey, ekRndA_, RndA_IV);
            //Shift RndA' back to RndA - Shift right
            byte[] RndAcard = new byte[8];
            for (int i = 1; i < RndA_.Length; i++)
            {
                RndAcard[i] = RndA_[i - 1];
            }
            RndAcard[0] = RndA_[RndAcard.Length - 1];

            //Compare RndAcard to RndA I generated
            var isAuth = RndAcard.ToHexString() == RndA.ToHexString() ? true : false;
            return isAuth;
        }

        public bool writeMoneroToCard(byte[] content)
        {
            //FF 00 00 00 09 D4 40 01 A2 04 AA BB CC DD 
            byte[] writeCommand = { 0xFF, 0x00, 0x00, 0x00, 0x09, 0xD4, 0x40, 0x01, 0xA2, 0x24 };

            byte[] newArray = new byte[14]; //Should be 14
            writeCommand.CopyTo(newArray, 0);
            newArray[10] = content[0];
            newArray[11] = content[1];
            newArray[12] = content[2];
            newArray[13] = content[3];

            writeCommand = newArray;

            var res = sendReaderCommand(writeCommand);
            if(res.Length != 10 || res.Substring(res.Length - 4) == "0900")
            {
                return false;
            }
            return true;
        }

        public BalanceUID getBalanceUid()
        {
            byte[] UIDcommand = { 0xFF, 0xCA, 0x00, 0x00, 0x00 }; //Serial NUmber (UID)
            var uid = sendReaderCommand(UIDcommand);
            var bl = uid.Remove(uid.Length - 4, 4);//remove 0900
            //24 is the page number - 24 is hex
            var k = authenticateCard();
            byte[] balCommand = { 0xFF, 0x00, 0x00, 0x00, 0x05, 0xD4, 0x40, 0x01, 0x30, 0x24 }; //Balance -> Read block of memory
            //Remove first six and last 4
            var b = sendReaderCommand(balCommand);
            if(b.Length < 10)
            {
                return null;
            }
            //Length should be greater than 6
            var balFrame = b.Remove(b.Length - 4, 4).Remove(0,6);
            if(balFrame.Length != 32)
            {
                return null;
            }
            //int balance = Convert.ToInt32(b);
            var page = balFrame.Remove(8); //Contents of 1 page (4 bytes)
            var balance = page.Remove(4); //First 2 bytes
            //Split
            StringBuilder sb = new StringBuilder();
            sb.Append(balance[2]);
            sb.Append(balance[3]);
            sb.Append(balance[0]);
            sb.Append(balance[1]);
            byte[] h = sb.ToString().ToHexBytes();

            if (BitConverter.IsLittleEndian)
                Array.Reverse(h); //need the bytes in the reverse order
            if (h.Length < 4)
            {
                byte[] temp = new byte[4];
                h.CopyTo(temp, 0);
                h = temp;
            }
            int restored = BitConverter.ToInt32(h, 0);
            return new BalanceUID
            {
                balance = restored,
                UID = uid
            };
        }

        public string sendReaderCommand(byte[] command)
        {
            byte[] response = new byte[300];
            reader.SetProtocol(0, Reader.ProtocolT1 | Reader.ProtocolT1);
            int responseLength = reader.Transmit(0, command, command.Length, response, response.Length);
            return response.ToHexString().Substring(0, responseLength * 2);
        }


        private void configureMQTT()
        {
            client = new MqttClient(IPAddress.Parse(MQTT_BROKER_IP));
            client.MqttMsgPublishReceived += Client_MqttMsgPublishReceived;
            string clientID = Guid.NewGuid().ToString();
            client.Connect(clientID);
            string[] topics = { "cashload" };
            byte[] qoslevel = { MqttMsgBase.QOS_LEVEL_AT_LEAST_ONCE };
            client.Subscribe(topics, qoslevel);
        }

        private void Client_MqttMsgPublishReceived(object sender, uPLibrary.Networking.M2Mqtt.Messages.MqttMsgPublishEventArgs e)
        {
            //throw new NotImplementedException();
        }

        public byte[] Encrypt3DES(byte[] keyBytes, byte[] tbe, byte[] IV)
        {
            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyBytes;
            tdes.Mode = CipherMode.CBC;
            tdes.IV = IV;
            tdes.Padding = PaddingMode.None;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(tbe, 0, tbe.Length);
            byte[] encrypted = new byte[resultArray.Length];
            for (int i = 0; i < resultArray.Length; i++)
            {

            }

            tdes.Clear();
            return resultArray;
        }


        public byte[] Decrypt3DES(byte[] keyBytes, byte[] tbd, byte[] IV)
        {
            var tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyBytes;
            tdes.Mode = CipherMode.CBC;
            tdes.IV = IV;
            tdes.Padding = PaddingMode.None;

            ICryptoTransform transformation = tdes.CreateDecryptor();
            byte[] resultArray = transformation.TransformFinalBlock(tbd, 0, tbd.Length);
            tdes.Clear();

            byte[] decrypted = new byte[resultArray.Length];
            for (int i = 0; i < resultArray.Length; i++)
            {
                byte num = resultArray[i];
                decrypted[i] = resultArray[i];
            }
            return decrypted;
        }

    }

    public static class Ext
    {

        public static string ToHexString(this byte[] hex)
        {
            if (hex == null) return null;
            if (hex.Length == 0) return string.Empty;

            var s = new StringBuilder();
            foreach (byte b in hex)
            {
                s.Append(b.ToString("x2"));
            }
            return s.ToString();
        }

        public static byte[] ToHexBytes(this string hex)
        {
            if (hex == null) return null;
            if (hex.Length == 0) return new byte[0];

            int l = hex.Length / 2;
            var b = new byte[l];
            for (int i = 0; i < l; ++i)
            {
                b[i] = Convert.ToByte(hex.Substring(i * 2, 2), 16);
            }
            return b;
        }

        public static bool EqualsTo(this byte[] bytes, byte[] bytesToCompare)
        {
            if (bytes == null && bytesToCompare == null) return true; // ?
            if (bytes == null || bytesToCompare == null) return false;
            if (object.ReferenceEquals(bytes, bytesToCompare)) return true;

            if (bytes.Length != bytesToCompare.Length) return false;

            for (int i = 0; i < bytes.Length; ++i)
            {
                if (bytes[i] != bytesToCompare[i]) return false;
            }
            return true;
        }

    }

    public class BalanceUID
    {
        public string UID { get; set; }
        public int balance { get; set; }
    }

    class UsbReciever : BroadcastReceiver
    {
        private static string ACTION_USB_PERMISSION = "TixiPOS.TixiPOS.TixiPOS.TixiPOS.USB_PERMISSION";
        public override void OnReceive(Context context, Intent intent)
        {
            string action = intent.Action;
            if (ACTION_USB_PERMISSION.Equals(action))
            {
                lock (this)
                {
                    UsbDevice device = (UsbDevice)intent
                            .GetParcelableExtra(UsbManager.ExtraDevice);

                    if (intent.GetBooleanExtra(
                            UsbManager.ExtraPermissionGranted, false))
                    {
                        if (device != null)
                        {
                            // call method to set up device communication
                        }
                    }
                    else
                    {

                    }
                }
            }
        }
    }


}